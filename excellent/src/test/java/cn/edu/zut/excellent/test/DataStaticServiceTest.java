package cn.edu.zut.excellent.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.edu.zut.excellent.entity.Student;
import cn.edu.zut.excellent.entity.Teacher;
import cn.edu.zut.excellent.entity.VirtualChoose;
import cn.edu.zut.excellent.supervisor.DaoFit;
import cn.edu.zut.excellent.supervisor.ServiceFit;

public class DataStaticServiceTest {
	private DaoFit daoFit;
	
	private ServiceFit serviceFit;

	@Before
	public void setUp() throws Exception {
		ApplicationContext context = new ClassPathXmlApplicationContext(
				new String[] { "classpath:conf/spring-sql.xml",
				"classpath:conf/spring-mvc.xml" });
		daoFit = (DaoFit) context.getBean("daoFit");
		serviceFit = (ServiceFit) context.getBean("serviceFit");
	}

//@SuppressWarnings("unchecked")
	//	@Test
//	public void test() {
//			Teacher teacher  = new Teacher();
//			teacher.setTeaId("6192");
//			teacher.setTeaName("郭丽");
//			teacher.setTeaTell("1231234");
//			int succ = daoFit.getTeacherDao().updateTea(teacher);
//			System.out.println(teacher);
////					
////	}
//	@Test
//	public void test1(){
//          String teaId="6192";
//		  List<Student> stuList=(List<Student>) serviceFit.getVirtualChooseService().getStuByteaId(teaId).getResult();
////          Iterator<Student> it=stuList.iterator();
//          while(it.hasNext()){
//        	  Student stu=it.next();
//        	  System.out.println(stu);	
//          }
//
//
//          
//	}
}


