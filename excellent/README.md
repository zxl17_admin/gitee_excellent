# 假如爱有天意

#### 项目介绍
当天边那颗星出现
　　你可知我又开始想念
　　有多少爱恋只能遥遥相望
　　就像月光洒向海面
　　年少的我们曾以为
　　相爱的人就能到永远
　　当我们相信情到深处在一起
　　听不见风中的叹息
　　谁知道爱是什么
　　短暂的相遇却念念不忘
　　用尽一生的时间
　　竟学不会遗忘
　　如今我们已天各一方
　　生活得像周围人一样
　　眼前人给我最信任的依赖
　　但愿你被温柔对待
　　多少恍惚的时候
　　仿佛看见你在人海川流
　　隐约中你已浮现
　　一转眼又不见
　　短暂的相遇却念念不忘
　　多少恍惚的时候
　　仿佛看见你在人海川流
　　隐约中你已浮现
　　一转眼又不见
　　当天边那颗星出现
　　你可知我又开始想念
　　有多少爱恋今生无处安放
　　冥冥中什么已改变
　　月光如春风拂面

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)